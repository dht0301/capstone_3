import React from "react";
import moment from "moment";
import "moment/locale/vi";
export default function MovieItemTab({ movie }) {
  return (
    <div className="flex mt-10 space-x-5">
      <img className="h-40 w-28 object-cover" src={movie.hinhAnh} alt="" />
      <div>
        <h3 className="font-medium text-2xl">{movie.tenPhim}</h3>
        <div className="grid grid-cols-3 gap-7">
          {movie.lstLichChieuTheoPhim.slice(0, 6).map((lichChieu) => {
            return (
              <p className="mt-2 bg-red-400 text-white px-3 py-2 rounded font-medium">
                {moment(lichChieu.ngayChieuGioChieu).format("LLL")}
              </p>
            );
          })}
        </div>
      </div>
    </div>
  );
}
