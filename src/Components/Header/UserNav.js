import React from "react";
import { useSelector } from "react-redux";
import { userLocalService } from "../../service/localService";
export default function UserNav() {
  let user = useSelector((state) => {
    return state.userReducer.user;
  });
  const handleLogout = () => {
    // Xóa dữ liệu từ localStore
    userLocalService.remove();
    // Đá user ra trang login
    // window.location.href = "/login";
    window.location.reload();
  };
  // console.log(`  🚀: user -> user`, user);
  const renderContent = () => {
    if (user) {
      // Da dang nhap
      return (
        <>
          <span>{user?.hoTen}</span>
          <button
            onClick={handleLogout}
            className="border-2 border-black px-5 py-2 rounded"
          >
            Dang Xuat
          </button>
        </>
      );
    } else {
      return (
        <>
          <button
            onClick={() => {
              window.location.href = "/login";
            }}
            className="border-2 border-black px-5 py-2 rounded"
          >
            Dang Nhap{" "}
          </button>
          <button className="border-2 border-black px-5 py-2 rounded">
            Dang Ky
          </button>
        </>
      );
    }
  };
  return <div>{renderContent()}</div>;
}
