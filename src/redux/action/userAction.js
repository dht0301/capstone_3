import { SET_USER_LOGIN } from "../constant/userContant";
import { postLogin } from "./../../service/userService";
import { message } from "antd";

export const setUserAction = (value) => {
  return {
    type: SET_USER_LOGIN,
    payload: value,
  };
};

export const SetUserActionService = (values, onSuccess) => {
  return (dispatch) => {
    postLogin(values)
      .then((res) => {
        message.success("dang nhap thanh cong");
        dispatch({
          type: SET_USER_LOGIN,
          payload: res.data.content,
        });
        onSuccess();
      })
      .catch((err) => {
        message.error("dang nhap that bai");
      });
  };
};
